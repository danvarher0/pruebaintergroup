Prueba InterGroup

Funcional en android y iOS, no cuenta con 2 items en la lista principal ya que en el APIRest no se describe algún Endpoint para obtener datos conjuntos como lo son:

- Capital/Región/Coordenadas.

Estos se obtienen de manera separada por endpoint y de preferencia se obtiene coordenadas ya que son necesarias para direccionar a la vista de mapa.
