import React from 'react';
import {
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from "@react-navigation/native";

import { Splash, Countries, MapCountry } from '../screens';

const Stack = createNativeStackNavigator();

const navigation = () => {
	return (
		<NavigationContainer>
			<Stack.Navigator
				screenOptions={{
					headerShown: false,
				}}
				initialRouteName={"Splash"}>
                <Stack.Screen name="Splash" component={Splash} />
				<Stack.Screen name="Countries" component={Countries} />
				<Stack.Screen name="MapCountry" component={MapCountry} />
			</Stack.Navigator>
		</NavigationContainer>
	)
}

export default navigation;