import React from "react";
import { 
    StyleSheet,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    Image,
    Animated
} from "react-native";

const Splash = ({ navigation }) => {

    setTimeout(() => {
        navigation.navigate("Countries");
    }, 2000);
    
    return (
        <View style={{ flex: 1, background: "#FFF", justifyContent: "center", alignItems: "center" }}>
            <Text>Loading...</Text>
        </View>
    )
}

export default Splash;