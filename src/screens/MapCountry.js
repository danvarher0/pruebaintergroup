import React, { useEffect, useState } from "react";
import { 
    StyleSheet,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    Image,
    Animated
} from "react-native";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";

import { GOOGLE_API_KEY } from "../constants/maps";

const MapCountries = ({ route, navigation }) => {

    const [region, setRegion] = useState(null);
    const [toLocation, setToLocation] = useState(null);

    useEffect(() => {
        let  { lat, long, currentLocation } = route.params;

        let mapRegion = {
            latitude:  lat,
            longitude: long,
            latitudeDelta: lat * 2,
            longitudeDelta: long * 2
        };

        let toLoc = {
            latitude: lat,
            longitude: long
        };

        setToLocation(toLoc);
        setRegion(mapRegion);

    }, []);

    function renderMap() {

        const countryMarker = () => (
            <Marker
                coordinate={toLocation}>
            </Marker>
        )

        const renderBackButton = () => (
                <TouchableOpacity
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: 50,
                        marginLeft:10,
                        width: 40,
                        height: 40,
                        borderWidth: 2,
                        borderRadius: 25

                    }}
                    onPress={() => {
                        navigation.goBack();
                    }}>
                    <Image
                        source={require("../assets/icons/back.png")}
                        resizeMode="contain"
                        style={{
                            width: 30,
                            height: 30
                        }} />
              </TouchableOpacity>
        )
        
        return (
            <View style={{ flex: 1 }}>
                <MapView 
                    provider={PROVIDER_GOOGLE}
                    initialRegion={region}
                    style={{ flex: 1 }}>
                        {countryMarker()}
                        {renderBackButton()}
                </MapView>
                

            </View>
        )
    }

    return (
        <View style={{ flex: 1 }}>
            {renderMap()}
        </View>
    )
}

export default MapCountries;