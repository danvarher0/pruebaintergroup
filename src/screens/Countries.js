import React, { useEffect, useState } from "react";
import { 
    StyleSheet,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    Image,
    Animated,
    FlatList
} from "react-native";

const Countries = ({ navigation }) => {

    const initialCurrentLocation = {
        streetName: "Maps",
        gps: {
            latitude: 1.5496614931250685,
            longitude: 110.36381866919922
        }
    }

    const [countries, setCountries] = useState([]);
    const [currentLocation, setCurrentLocation] = useState(initialCurrentLocation)

    const getCountries = async () => {
        try {
            const response = await fetch('https://countriesnow.space/api/v0.1/countries/positions');
            const json = await response.json();
            setCountries(json.data);
          } catch (error) {
            console.error(error);
          } finally {
            setLoading(false);
        }

    }

    useEffect(() => {
        getCountries();
    }, []);

    function renderCountriesList() {
        
        const renderItem = ({ item }) => (
            <View
                style={{ 
                    padding: 20,
                    marginBottom: 10,
                    borderRadius: 10,
                    backgroundColor: "#FFF",
                    ...styles.shadow
                }}>
                    <View 
                        style={{
                            flexDirection: "row",
                            alignItems: "center"
                        }}>
                        <TouchableOpacity
                            style={{
                                width: 50,
                                height: 50,
                                borderRadius: 10,
                                alignItems: "center",
                                justifyContent: "center",
                                backgroundColor: "#D4D4D4"
                            }}
                            onPress={() => {
                                navigation.navigate("MapCountry", {
                                    lat: item.lat,
                                    long: item.long,
                                    currentLocation: currentLocation
                                });
                            }}>
                                <Text style={{ fontSize: 25 }}>GO</Text>
                        </TouchableOpacity>
                        <View
                            style={{ 
                                flexDirection: "row",
                                marginStart: 20
                            }}>
                                <Text style={{ color: "Black" }}>{item.name}</Text>   
                        </View>
                    </View>
            </View>
        )
        return (
            <FlatList
                data={countries}
                keyExtractor={item => `${item.id}`}
                renderItem={renderItem}
                contentContainerStyle={{
                    paddingHorizontal: 10,
                    paddingBottom: 30
                }} />
        )
    }
    return (
        <SafeAreaView style={styles.container}>
            {renderCountriesList()}
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF"
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        elevation: 1
    }
})

export default Countries;